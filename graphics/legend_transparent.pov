// ******************************************************
// Created by Jmol 14.19.1  2017-06-25 16:37
//
// This script was generated on Fri, 30 Jun 2017 22:29:41 +0100
// ******************************************************

/****** Jmol Embedded Script **** 
# Jmol state version 14.19.1  2017-06-25 16:37;

function _setWindowState() {
# preferredWidthHeight -1 -1;
# width -1;
# height -1;
  stateVersion = 1419001;
  background [xffffff];
  axis1Color = "[xff0000]";
  axis2Color = "[x008000]";
  axis3Color = "[x0000ff]";
  set ambientPercent 45;
  set diffusePercent 84;
  set specular true;
  set specularPercent 22;
  set specularPower 40;
  set specularExponent 6;
  set celShading false;
  set celShadingPower 10;
  set zShadePower 3;
}

function _setFileState() {

  set allowEmbeddedScripts false;
  set appendNew true;
  set appletProxy "";
  set applySymmetryToBonds false;
  set autoBond true;
  set bondRadiusMilliAngstroms 150;
  set bondTolerance 0.45;
  set defaultLattice {0.0 0.0 0.0};
  set defaultLoadFilter "";
  set defaultLoadScript "";
  set defaultStructureDssp true;
  set defaultVDW Auto;
  set forceAutoBond false;
  #set defaultDirectory "/home/james";
  #set loadFormat "https://files.rcsb.org/download/%FILE.pdb";
  #set loadLigandFormat "https://files.rcsb.org/ligands/download/%FILE.cif";
  #set smilesUrlFormat "https://cactus.nci.nih.gov/chemical/structure/%FILE/file?format=sdf&get3d=true";
  #set nihResolverFormat "https://cactus.nci.nih.gov/chemical/structure/%FILE";
  #set pubChemFormat "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/%FILE/SDF?record_type=3d";
  #set edsUrlFormat "http://eds.bmc.uu.se/eds/dfs/%c2%c3/%file/%file.omap";
  #set edsUrlFormatDiff "http://eds.bmc.uu.se/eds/dfs/%c2%c3/%file/%file_diff.omap";
  #set edsUrlCutoff "http://eds.bmc.uu.se/eds/dfs/%c2%c3/%file/%file.sfdat";
  set bondingVersion 0;
  set legacyAutoBonding false;
  set legacyAutoBonding false;
  set legacyHAddition false;
  set legacyJavaFloat false;
  set minBondDistance 0.4;
  set minimizationCriterion  0.001;
  set minimizationSteps  100;
  set multipleBondBananas false;
  set pdbAddHydrogens false;
  set pdbGetHeader false;
  set pdbSequential false;
  set percentVdwAtom 23;
  set smallMoleculeMaxAtoms 40000;
  set smartAromatic true;
  load /*file*/"./legend.pdb";

}

function _setParameterState() {

   set defaultanglelabel "%VALUE %UNITS";
   set defaultcolorscheme "jmol";
   set defaultdistancelabel "%VALUE %UNITS";
   set defaultdrawarrowscale 0.5;
   set defaultlabelpdb "%m%r";
   set defaultlabelxyz "%a";
   set defaultlattice "{0 0 0}";
   set defaultloadfilter "";
   set defaultloadscript "";
   set defaulttorsionlabel "%VALUE %UNITS";
   set defaulttranslucent 0.5;
   set defaultvdw "Auto";
  color aluminum [x8c8279];
  color calcium [xb5bd00];
  color hydrogen [x000000];
  color oxygen [xff0000];
  color potassium [x002855];
  color silicon [xf6be00];
  color sodium [x0000a7];
  set allowembeddedscripts true;
  set allowmoveatoms false;
  set allowrotateselected false;
  set animationmode "once";
  set appletproxy "";
  set applysymmetrytobonds false;
  set atomtypes "";
  set autobond true;
  set autofps false;
  set axes window;
  set axesmode 0;
  set axesoffset 0.0;
  set axesscale 2.0;
  set bondmodeor false;
  set bondradiusmilliangstroms 150;
  set bondtolerance 0.45;
  set cartoonbaseedges false;
  set cartoonblockheight 0.5;
  set cartoonblocks false;
  set cartoonfancy false;
  set cartoonladders false;
  set cartoonrockets false;
  set cartoonsteps false;
  set chaincasesensitive false;
  set dataseparator "~~~";
  set defaultstructuredssp true;
  set delaymaximumms 0;
  set dipolescale 1.0;
  set disablepopupmenu false;
  set displaycellparameters true;
  set dotdensity 3;
  set dotscale 1;
  set dotsselectedonly false;
  set dotsurface true;
  set dragselected false;
  set drawfontsize 14.0;
  set drawhover false;
  set dsspcalculatehydrogenalways true;
  set edsurlformatdiff "http://eds.bmc.uu.se/eds/dfs/%c2%c3/%file/%file_diff.omap";
  set ellipsoidarcs false;
  set ellipsoidarrows false;
  set ellipsoidaxes false;
  set ellipsoidaxisdiameter 0.02;
  set ellipsoidball true;
  set ellipsoiddotcount 200;
  set ellipsoiddots false;
  set ellipsoidfill false;
  set energyunits "kJ";
  set forceautobond false;
  set gestureswipefactor 1.0;
  set greyscalerendering false;
  set hbondsangleminimum 90.0;
  set hbondsbackbone false;
  set hbondsdistancemaximum 3.25;
  set hbondsrasmol true;
  set hbondssolid false;
  set helixstep 1;
  set helppath "http://chemapps.stolaf.edu/jmol/docs/index.htm";
  set hermitelevel 0;
  set hiddenlinesdashed false;
  set hidenameinpopup false;
  set hidenavigationpoint false;
  set highresolution false;
  set hoverdelay 0.5;
  set isosurfacekey false;
  set isosurfacepropertysmoothing true;
  set isosurfacepropertysmoothingpower 7;
  set justifymeasurements false;
  set loadatomdatatolerance 0.01;
  set measureallmodels false;
  set measurementlabels true;
  set meshscale 1;
  set messagestylechime false;
  set minbonddistance 0.4;
  set minimizationcriterion 0.001;
  set minimizationrefresh true;
  set minimizationsilent false;
  set minimizationsteps 100;
  set minpixelselradius 6;
  set modulationscale 1.0;
  set monitorenergy false;
  set multiplebondbananas false;
  set multiplebondradiusfactor 0.0;
  set multiplebondspacing -1.0;
  set navigationperiodic false;
  set navigationspeed 5.0;
  set nbocharges true;
  set nmrpredictformat "http://www.nmrdb.org/service/predictor?POST?molfile=";
  set nmrurlformat "http://www.nmrdb.org/new_predictor?POST?molfile=";
  set partialdots false;
  set particleradius 20.0;
  set pdbaddhydrogens false;
  set pdbgetheader false;
  set pdbsequential false;
  set percentvdwatom 23;
  set pickingspinrate 10;
  set pointgroupdistancetolerance 0.2;
  set pointgrouplineartolerance 8.0;
  set propertyatomnumbercolumncount 0;
  set propertyatomnumberfield 0;
  set propertycolorscheme "roygb";
  set propertydatacolumncount 0;
  set propertydatafield 0;
  set quaternionframe "p";
  set rangeselected false;
  set ribbonaspectratio 16;
  set ribbonborder false;
  set rocketbarrels false;
  set selecthetero true;
  set selecthydrogen true;
  set sheetsmoothing 1.0;
  set showhiddenselectionhalos false;
  set showhydrogens true;
  set showmeasurements true;
  set showmodulationvectors false;
  set showmultiplebonds true;
  set shownavigationpointalways false;
  set showunitcelldetails true;
  set slabbyatom false;
  set slabbymolecule false;
  set smallmoleculemaxatoms 40000;
  set smartaromatic true;
  set solventprobe false;
  set solventproberadius 1.2;
  set ssbondsbackbone false;
  set starwidth 0.05;
  set stereodegrees -5;
  set strandcountformeshribbon 7;
  set strandcountforstrands 5;
  set strutdefaultradius 0.3;
  set strutlengthmaximum 7.0;
  set strutsmultiple false;
  set strutspacing 6;
  set tracealpha true;
  set translucent true;
  set twistedsheets false;
  set usenumberlocalization true;
  set vectorscale 1.0;
  set vectorscentered false;
  set vectorsymmetry false;
  set vectortrail 0;
  set vibrationscale 0.5;
  set waitformoveto true;
  set wireframerotation false;
  set zdepth 0;
  set zoomheight false;
  set zoomlarge true;
  set zslab 50;

# label defaults;
  select none;
  color label none;
  background label none;
  set labelOffset 4 4;
  set labelAlignment left;
  set labelPointer off;
  font label 13.0 SansSerif Plain;

}

function _setModelState() {

  select ({2});
  Spacefill 0.522;
  select ({6});
  Spacefill 0.423;
  select ({5});
  Spacefill 0.4825;
  select ({3});
  Spacefill 0.531;
  select ({1});
  Spacefill 0.3495;
  select ({4});
  Spacefill 0.6325;
  select ({0});
  Spacefill 0.253;

  axes off;
  font axes 14.0 SansSerif Plain;
  axes off;
  axes scale 2.0;

  frank off;
  font frank 16.0 SansSerif Plain;
  select *;
  set fontScaling false;

}

function _setPerspectiveState() {
  set perspectiveModel 11;
  set scaleAngstromsPerInch 0.0;
  set perspectiveDepth true;
  set visualRange 5.0;
  set cameraDepth 3.0;
  boundbox corners {0.0 0.0 0.0} {0.0 30.0 0.0} # volume = 0.0;
  center {0.0 15.0 0.0};
   moveto -1.0 {0 0 1 0} 100.0 0.0 0.0 {0.0 15.0 0.0} 17.05 {0 0 0} 0 0 0 3.0 0.0 0.0;
  save orientation "default";
  moveto 0.0 { -602 -572 -557 119.12} 100.0 0.0 0.0 {0.0 15.0 0.0} 17.05 {0 0 0} 0 0 0 3.0 0.0 0.0;;
  slab 100;depth 0;
  set slabRange 0.0;
  set spinX 0; set spinY 30; set spinZ 0; set spinFps 30;  set navX 0; set navY 0; set navZ 0; set navFps 10;
}

function _setSelectionState() {
  select ({0:6});
  set hideNotSelected false;
}

function _setState() {
  initialize;
  set refreshing false;
  _setWindowState;
  _setFileState;
  _setParameterState;
  _setModelState;
  _setPerspectiveState;
  _setSelectionState;
  set refreshing true;
  set antialiasDisplay true;
  set antialiasTranslucent true;
  set antialiasImages false;
}

_setState;

**/
// Jmol perspective:
// screen width height dim: 1362 602 1362
// perspectiveDepth: true
// cameraDistance(angstroms): 119.34999
// aperatureAngle(degrees): 16.260204
// scalePixelsPerAngstrom: 39.8827
// light source: {-0.34815532, -0.34815532, 0.87038827}
// lighting:   set ambientPercent 45;   set diffusePercent 84;   set specular true;   set specularPercent 22;   set specularPower 40;   set specularExponent 6;   set celShading false;   set celShadingPower 10;   set zShadePower 3;   set zDepth 0;   set zSlab 50;   set zShade false; 
// center: {0.0, 15.0, 0.0}
// rotationRadius: 17.05
// boundboxCenter: {0.0, 15.0, 0.0}
// translationOffset: 
// zoom: 100.0
// moveto command: moveto 1.0 { -602 -572 -557 119.12} 100.0 0.0 0.0 {0.0 15.0 0.0} 17.05 {0 0 0} 0 0 0 3.0 0.0 0.0;

// ******************************************************
// Declare the resolution, camera, and light sources.
// ******************************************************

// NOTE: if you plan to render at a different resolution,
// be sure to update the following two lines to maintain
// the correct aspect ratio.

#declare Width = 1362;
#declare Height = 602;
#declare minScreenDimension = 602;
#declare showAtoms = true;
#declare showBonds = true;
#declare noShadows = true;
camera{
  perspective
  angle 16.260204
  right < 1362, 0, 0>
  up < 0, -602, 0 >
  sky < 0, -1, 0 >
  location < 681.0, 301.0, 0>
  look_at < 681.0, 301.0, 1000 >
}

background { color rgb <1.0,1.0,1.0> }

light_source { <-474.18753,-474.18753, -1185.4689>  rgb <0.6,0.6,0.6> }


// ***********************************************
// macros for common shapes
// ***********************************************

#default { finish {
  ambient 0.45
  diffuse 0.84
  specular 0.22
  roughness .00001
  metallic
  phong 0.9
  phong_size 120
}}

#macro check_shadow()
 #if (noShadows)
  no_shadow 
 #end
#end

#declare slabZ = 0;
#declare depthZ = 2147483647;
#declare dzSlab = 10;
#declare dzDepth = dzSlab;
#declare dzStep = 0.001;

#macro clip()
  clipped_by { box {<0,0,slabZ>,<Width,Height,depthZ>} }
#end

#macro circleCap(Z,RADIUS,R,G,B,T)
// cap for lower clip
 #local cutDiff = Z - slabZ;
 #local cutRadius2 = (RADIUS*RADIUS) - (cutDiff*cutDiff);
 #if (cutRadius2 > 0)
  #local cutRadius = sqrt(cutRadius2);
  #if (dzSlab > 0)
   #declare dzSlab = dzSlab - dzStep;
  #end
  cylinder{<X,Y,slabZ-dzSlab>,<X,Y,(slabZ+1)>,cutRadius
   pigment{rgbt<R,G,B,T>}
   translucentFinish(T)
   check_shadow()}
 #end
// cap for upper clip
 #declare cutDiff = Z - depthZ;
 #declare cutRadius2 = (RADIUS*RADIUS) - (cutDiff*cutDiff);
 #if (cutRadius2 > 0)
  #local cutRadius = sqrt(cutRadius2);
  #if (dzDepth > 0)
   #declare dzDepth = dzDepth - dzStep;
  #end
  cylinder{<X,Y,depthZ+dzDepth>,<X,Y,(depthZ-1)>,cutRadius
   pigment{rgbt<R,G,B,T>}
   translucentFinish(T)
   check_shadow()}
 #end
#end

#macro translucentFinish(T)
 #local shineFactor = T;
 #if (T <= 0.25)
  #declare shineFactor = (1.0-4*T);
 #end
 #if (T > 0.25)
  #declare shineFactor = 0;
 #end
 finish {
  ambient 0.45
  diffuse 0.84
  specular 0.22
  roughness .00001
  metallic shineFactor
  phong 0.9*shineFactor
  phong_size 120*shineFactor
}#end

#macro a(X,Y,Z,RADIUS,R,G,B,T)
 sphere{<X,Y,Z>,RADIUS
  pigment{rgbt<R,G,B,T>}
  translucentFinish(T)
  clip()
  check_shadow()}
#end

#macro q(XX,YY,ZZ,XY,XZ,YZ,X,Y,Z,J,R,G,B,T)
 quadric{<XX,YY,ZZ>,<XY,XZ,YZ>,<X,Y,Z>,J
  pigment{rgbt<R,G,B,T>}
  translucentFinish(T)
  clip()
  check_shadow()}
#end

#macro b(X1,Y1,Z1,RADIUS1,X2,Y2,Z2,RADIUS2,R,G,B,T)
 cone{<X1,Y1,Z1>,RADIUS1,<X2,Y2,Z2>,RADIUS2
  pigment{rgbt<R,G,B,T>}
  translucentFinish(T)
  clip()
  check_shadow()}
#end

#macro c(X1,Y1,Z1,RADIUS1,X2,Y2,Z2,RADIUS2,R,G,B,T)
 cone{<X1,Y1,Z1>,RADIUS1,<X2,Y2,Z2>,RADIUS2 open
  pigment{rgbt<R,G,B,T>}
  translucentFinish(T)
  clip()
  check_shadow()}
#end

a(83.0,300.0,4728.0,10.0,0.0,0.0,0.0,0)
a(282.0,300.0,4739.0,13.5,1.0,0.0,0.0,0)
a(481.0,300.0,4749.0,20.5,0.671875,0.36328125,0.94921875,0)
a(681.0,301.0,4760.0,21.0,0.7109375,0.7421875,0.0,0)
a(880.0,301.0,4770.0,25.0,0.0,0.16015625,0.3359375,0)
a(1079.0,301.0,4780.0,19.0,0.96484375,0.74609375,0.0,0)
a(1278.0,301.0,4791.0,16.5,0.55078125,0.51171875,0.4765625,0)
