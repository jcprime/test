@SET COORD_FILE                     scol17_N6.cp2k
@SET LENGTHS                        18.114152 18.537615 6.535136
@SET ANGLES                         90.154147 90.487534 90.019738
@SET MULTICELL                      1 1 2
@SET BASIS_FILE                     /home/james/quickstep/data/BASIS_MOLOPT
@SET PSEUDO_FILE                    /home/james/quickstep/data/GTH_POTENTIALS
@SET RESTART                        FALSE
@IF ( ${RESTART} == TRUE )
    @SET PRINT_RESTART              ON
    @SET RESTART_FILE               scol17_N6-1.restart
    &EXT_RESTART
	    RESTART_FILE_NAME           ${RESTART_FILE}
    &END EXT_RESTART
    @SET SCF_GUESS                  RESTART
@ENDIF
@IF ( ${RESTART} == FALSE )
    @SET PRINT_RESTART              ON
    @SET SCF_GUESS                  ATOMIC
@ENDIF
@SET CONVERGE_TO                    1.0E-006
@SET SCF_INNER_CONVERGENCE          1.0E-003
@SET SCF_OUTER_CONVERGENCE          1.0E-004
@SET SCF_MINIMISER                  CG
@SET SCF_PRECONDITIONER             FULL_SINGLE_INVERSE
@SET XC                             PBE
@SET SCF_INNER_MAX                  1000
@SET SCF_OUTER_MAX                  5000
@SET CUTOFF                         750
@SET REL_CUTOFF                     90
@SET EXTRAPOLATION                  ASPC

&GLOBAL
	PROJECT                         scol17_N6
	RUN_TYPE                        CELL_OPT
	PRINT_LEVEL                     LOW
	TRACE                           OFF
	&TIMINGS
		THRESHOLD                   0.02
	&END TIMINGS
    WALLTIME                        1209600
&END GLOBAL
&FORCE_EVAL
	METHOD                          QUICKSTEP
    STRESS_TENSOR                   ANALYTICAL
	&SUBSYS
		&CELL
			ABC                     ${LENGTHS}
			ALPHA_BETA_GAMMA        ${ANGLES}
            MULTIPLE_UNIT_CELL      ${MULTICELL}
		&END CELL
		&TOPOLOGY
			COORD_FILE_FORMAT       CP2K
			COORD_FILE_NAME         ${COORD_FILE}
            MULTIPLE_UNIT_CELL      ${MULTICELL}
		&END TOPOLOGY
# Switched to lower basis sets with MOLOPT using Google Group's guidance (Cholesky errors strike again)
		&KIND H
			ELEMENT H
			BASIS_SET DZVP-MOLOPT-SR-GTH-q1
			POTENTIAL GTH-PBE-q1
		&END KIND
		&KIND O1
			ELEMENT O
			BASIS_SET DZVP-MOLOPT-SR-GTH-q6
			POTENTIAL GTH-PBE-q6
		&END KIND
		&KIND O2
			ELEMENT O
			BASIS_SET DZVP-MOLOPT-SR-GTH-q6
			POTENTIAL GTH-PBE-q6
		&END KIND
		&KIND Si
			ELEMENT Si
			BASIS_SET DZVP-MOLOPT-SR-GTH-q4
			POTENTIAL GTH-PBE-q4
		&END KIND
		&KIND Al
			ELEMENT Al
			BASIS_SET DZVP-MOLOPT-SR-GTH-q3
			POTENTIAL GTH-PBE-q3
		&END KIND
		&KIND Na
			ELEMENT Na
			BASIS_SET DZVP-MOLOPT-SR-GTH-q9
			POTENTIAL GTH-PBE-q9
		&END KIND
		&KIND Ca
			ELEMENT Ca
			BASIS_SET DZVP-MOLOPT-SR-GTH-q10
			POTENTIAL GTH-PBE-q10
		&END KIND
		&KIND K
			ELEMENT K
			BASIS_SET DZVP-MOLOPT-SR-GTH-q9
			POTENTIAL GTH-PBE-q9
		&END KIND
		&KIND Mg
			ELEMENT Mg
			BASIS_SET DZVP-MOLOPT-SR-GTH-q10
			POTENTIAL GTH-PBE-q2
		&END KIND
        &PRINT
            &ATOMIC_COORDINATES MEDIUM
#                ADD_LAST            SYMBOLIC
                &EACH
                    CELL_OPT         1
                &END EACH
                FILENAME            coords
            &END ATOMIC_COORDINATES
            &CELL MEDIUM
#                ADD_LAST            SYMBOLIC
                &EACH
                    CELL_OPT         1
                &END EACH
                FILENAME            cellparam
            &END CELL
        &END PRINT
	&END SUBSYS
	&DFT
		BASIS_SET_FILE_NAME         ${BASIS_FILE}   
		POTENTIAL_FILE_NAME         ${PSEUDO_FILE}
		&QS
			EPS_DEFAULT             ${CONVERGE_TO}
			EXTRAPOLATION           ${EXTRAPOLATION}
			EXTRAPOLATION_ORDER     3
			MAP_CONSISTENT          TRUE
		&END QS
		&MGRID
			CUTOFF                  ${CUTOFF}
			REL_CUTOFF              ${REL_CUTOFF}
		&END MGRID
		&SCF
			SCF_GUESS               ${SCF_GUESS}
            EPS_SCF                 ${SCF_INNER_CONVERGENCE}
			MAX_SCF                 ${SCF_INNER_MAX}
			&OUTER_SCF
                EPS_SCF             ${SCF_OUTER_CONVERGENCE}
				MAX_SCF             ${SCF_OUTER_MAX}
			&END OUTER_SCF
			&OT                     ON
				MINIMIZER           ${SCF_MINIMISER}
# Only way I could get it working without Cholesky decomposition errors was by having preconditioner set to NONE
				PRECONDITIONER      ${SCF_PRECONDITIONER}
				ENERGY_GAP          0.001
			&END OT
			&PRINT
				&RESTART            ${PRINT_RESTART}
				&END RESTART
			&END PRINT
		&END SCF
        &XC
            &XC_FUNCTIONAL          ${XC}
            &END XC_FUNCTIONAL
        &END XC
	&END DFT
&END FORCE_EVAL
# Most things in MOTION section are the defaults
&MOTION
    &PRINT
#        &CELL HIGH
#            ADD_LAST                SYMBOLIC
#            &EACH
#                CELL_OPT            1
#                GEO_OPT             1
#                QS_SCF              1
#            &END EACH
#            FILENAME                cell
#        &END CELL
#        &STRUCTURE_DATA HIGH
#            ADD_LAST                SYMBOLIC
#            &EACH
#                CELL_OPT            1
#                GEO_OPT             1
#                QS_SCF              1
#            &END EACH
#            FILENAME                structure
#            POSITION
#        &END STRUCTURE_DATA
        &TRAJECTORY LOW
            ADD_LAST                SYMBOLIC
            FILENAME                trajectory
            FORMAT                  PDB
        &END TRAJECTORY
    &END PRINT
    &CELL_OPT
        TYPE        DIRECT_CELL_OPT
        MAX_DR      1.0E-02
        MAX_FORCE   1.0E-03
        RMS_DR      1.0E-02
        RMS_FORCE   1.0E-03
        MAX_ITER    10000
        OPTIMIZER   CG
    &END CELL_OPT
	&GEO_OPT
		TYPE        MINIMIZATION
		MAX_DR      1.0E-02
		MAX_FORCE   1.0E-03
		RMS_DR      1.0E-02
		RMS_FORCE   1.0E-03
		MAX_ITER    10000
		OPTIMIZER   CG
		&CG
			MAX_STEEP_STEPS 0
			RESTART_LIMIT   9.0E-01
		&END CG
	&END GEO_OPT
&END MOTION
