#!/bin/bash
# directory_create.sh

# The following line should go in a for loop
#step="${dir%%_*}" && step="${step##[a-z]*[^0-9]}"; echo "$step"

# For example:
cd ~/test/coords/nat || exit
for dir in *; do
    step="${dir%%_*}" && step="${step##[a-z]*[^0-9]}"
    #echo "$step"
    if [ ! -d "$step" ]; then
        echo "Making $step directory" && mkdir "$step"
    fi
    mv "$dir" "$step"/
done
