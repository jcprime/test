#!/bin/bash
# setting up the environment for SGE:
#$ -cwd -V
#$ -m e
#$ -M james@master.xenon.local
#$ -pe smp 4 
#$ -N baur1996
#$ -e baur1996.error
#$ -o baur1996.output

/opt/openmpi/gfortran/1.6.5/bin/mpirun --mca btl ^tcp -n 4 /usr/local/cp2k/2.6.0/bin/cp2k.popt baur1996.inp > baur1996.log

