# Processing output of CP2K

Assumptions made:

* External file for coordinates (not all coordinates listed in input file)
* CP2K format for coordinates
* PDB format for iterations (all append to the same `*-trajectory-pos-1.pdb` file, it seems by default)
* PyMOL will be used to produce images of the final structures at each stage
* CSV or similar file will be used to hold all important information for later data processing (with R in my case)
* ImageMagick is installed and working on your (Linux-y) machine

## Grabbing the final step's coordinates from the all-steps-appended PDB file

Given that the coordinates (and other crystallographic information) are separated by `REMARK` lines in the Big File, I wrote a tiny bash script which takes the input (Big) file name as an argument, and uses awk to find the last instance of `REMARK` in the input file, and then outputs the lines from that point onwards (via another awk loop) to the newly-created output file:

```bash
#!/bin/bash

orig_file="$1"
output_file="${orig_file%%-*}-final.pdb"

lineno=$(awk '/REMARK/ {var=NR} END{print var}' "$orig_file")
awk -v lineno="$lineno" 'NR>=lineno' "$orig_file" > "$output_file"
```

Given an input filename of `nat13_N1-trajectory-pos-1.pdb` (`/path/to/scriptname.sh nat13_N1-trajectory-pos-1.pdb`), the above would output the final step's coordinates to a newly-created `nat13_N1-final.pdb`.

## Calculating the things you need in the CSV file that aren't included in the PDB files

I took a leaf out of `general_info.pl`'s book (which can be found in many places belonging to Xenon users!) in what to output to the CSV file:

* Filename
* -Optimised?-
* Energy
* -Gnorm-
* A
* B
* C
* Alpha
* Beta
* Gamma
* Volume
* -Pressure * Volume-

Everything in the list above - excluding those struck through, of course - can be found in the PDB files, except volume.  So I wrote a Python script to calculate this for me.

I also wrote a script to grab the filenames and energies only, which contains the following:

```bash
if [[ "$#" -eq "3" ]]; then
    log_file="$1"
    pdb_file="$2"
    output_file="$3"
elif [[ "$#" -eq "2" ]]; then
    log_file="$1"
    output_file="$2"
fi

inpath="$(realpath "$log_file")"
outpath="$(realpath "$output_file")"

# Append the filename and the last instance of the matched regex in specific format for ease of reading to the given output (.ssv) file
awk '/ENERGY\| Total/ {ene=$9} END{printf "%-30s%25.15f\n", FILENAME, ene}' "$log_file" >> "$output_file"

cat "$output_file" | sort -n -k2 | uniq > "${output_file%%.ssv}.tmp" && cp ${output_file%%.ssv}.tmp "$output_file" && rm "${output_file%%.ssv}.tmp"
```

Which should be used from within the zeolite-name directory as follows (as an example):

```bash
for num in {01..15}; do
    cd "$num"
    for dir in nat*; do
        if [ -d "$dir" ]; then
            energy_extract.sh "$dir"/"$dir".log "${dir%%_*}".ssv
        fi
    done
    mv "${dir%%_*}".ssv ../ene/
    cd ..
done
```

Then the easiest way to deal with the full cell-stuff-grabbing would be to get the last step in the cumulative PDB files (i.e. the `*-final.pdb` file in each directory), squidging them all into a separate (`pdb`) directory, then running `cellvolumes.py` from within that directory.  The current version (see `APPENDIX.md` for snapshot) will take each file and append it to a step-specific `*_cellvol.ssv` file, and then run:

```bash
for file in *_cellvol.ssv; do
    cat "$file" >> nat_full_cellvol.ssv
done
```

to append all those step-specific files to one big everything-include-y file.

Things to note that might need tweaking:

* If your CP2K input file contains a `MULTICELL` section where you make a supercell along one or more axes, the output coordinates and restart files will appear to be larger than your initial coordinates would suggest, because CP2K will include duplicate-but-translated cells as being one big new cell.  Your script will need to account for this in the volume calculations.

