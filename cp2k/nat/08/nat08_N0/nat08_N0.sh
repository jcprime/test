#!/bin/bash
# setting up the environment for SGE:
#$ -cwd -V
#$ -m e
#$ -M james@master.xenon.local
#$ -pe smp 4 
#$ -N nat08_N0
#$ -e nat08_N0.error
#$ -o nat08_N0.output

/opt/openmpi/gfortran/1.6.5/bin/mpirun --mca btl ^tcp -n 4 /usr/local/cp2k/2.6.0/bin/cp2k.popt nat08_N0.inp > nat08_N0.log

