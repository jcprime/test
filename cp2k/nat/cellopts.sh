#!/bin/bash

for dir in [0-9]*; do
    dir="$(basename "$dir")"
    if [ -d "$dir" ]; then
        for subdir in "$dir"/nat*; do
            subdir="$(basename "$subdir")"
            if [ -d "$dir/$subdir" ]; then
                #echo "Make $dir/$subdir/cellopt directory"
                #echo "Copy template.inp to $dir/$subdir/cellopt/$subdir.inp"
                #echo "Copy $dir/$subdir/$subdir.cart to $dir/$subdir/cellopt/$subdir.cp2k"
                mkdir -p "$dir/$subdir/cellopt"
                cp template.inp "$dir/$subdir/cellopt/$subdir.inp"
                cp "$dir/$subdir/$subdir.cart" "$dir/$subdir/cellopt/$subdir.cp2k"
            fi
        done
    fi
done
