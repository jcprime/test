#!/bin/bash
# sortinpfiles.sh
# Takes input files from ~/data/cp2k/nat directory and copies them into their respective intended directories

for file in nat[0-9][0-9]*.inp; do
    num="${file##nat}" && num="${num%%[_.]*inp}"
    name="${file%%.inp}"
    cp "$file" "$num/$name/"
done
