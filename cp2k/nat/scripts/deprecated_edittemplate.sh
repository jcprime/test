#!/bin/bash
# edit_template.sh
# Taken from cutoff_tests/cutoff_inputs.sh for the variable-replacement inline-editing code

zeo="nat"
path="/home/james/quickstep/$zeo"
gulp_path="/home/james/data/gulp/$zeo"
template_file="/home/james/quickstep/$zeo/template.inp"

scf_guess="ATOMIC"
converge_to="1.0E-006"
inner_convergence="1.0E-003"
outer_convergence="1.0E-004"
minimiser="CG"
preconditioner="FULL_SINGLE_INVERSE"
xc="PBE"
max_inner_cycles="1000"
max_outer_cycles="5000"
cutoff="750"
rel_cutoff="60"
extrapolation="ASPC" # Or something
run_type="GEOMETRY_OPTIMIZATION"

# Make directories (per number of waters lost) and subdirectories (per file to be tested)
for num in {00..24}; do
    # If corresponding numbered directory does not exist in gulp_path, skip to the next iteration
    if [[ ! -d "${gulp_path}/$num" ]]; then
        #rmdir "$path/$num"
        continue
    fi
    # Make directory for number of waters lost
    if [[ ! -d "$path/$(printf '%02d' "$((10#$num))")" ]]; then
        mkdir -p "$path/$(printf '%02d' "$((10#$num))")"
    fi
    for file in "${gulp_path}/$num/"*.gin; do
        # Make directory for given .gin file (in gulp_path/num/ directory) in path/num/ directory
        gulp_file="${file##*/}" && gulp_file="${gulp_file%%.*}" # Gives e.g. lau03_N12
        #echo "${gulp_file}"
        if [[ ! -d "$path/"$num/"${gulp_file}" ]]; then
            mkdir -p "$path/$num/${gulp_file}"
        fi
        # Copy original .gin file from original location into newly-created directory
        cp "$file" "$path/$num/${gulp_file}/${gulp_file}.gin"
        
        cd "${path}/${num}/${gulp_file}" || continue
        # Conversion script for coordinates on gulp_file and grab the relevant stuff (abc and angles)
        gin_file="${gulp_file}.gin" # Gives e.g. lau03_N12.gin
        coordinate_file="${gulp_file}.cp2k" # Gives e.g. lau03_N12.cp2k
        # Grab cell parameters into a new variable
        cell=$(grep -A1 'cell' "${gin_file}" | tail -n 1 | tr -s ' ')
        echo "Cell parameters: ${cell}"
        # Create temporary file
        temp_file="${gulp_file}.tmp" # Gives e.g. lau03_N12.tmp
        > "${temp_file}"
        # THIS IS SPECIFIC TO UNIX/LINUX SYSTEMS, AND WON'T WORK ON MAC OS X:
        # Output everything including "fractional" and "totalenergy" OR "species" (whichever comes first) to a temporary file for processing in the while loop to follow
        sed -n "/^fractional/,/^\(space\|totalenergy\|species\)/p" "${gin_file}" >> "${temp_file}"
        frac_file="${gulp_file}.frac" # Gives e.g. lau03_N12.frac
        > "${frac_file}"
        # Remove first and last lines using sed
        #sed '1d;$d' "${temp_file}"
        awk ' { if ($1 ~ /^[0-9].*/) { print $1, $2, $3, $4; next } else if ($1 ~ /^[A-Za-z].*/ && $2 ~ "core") { print $1, $3, $4, $5; next } } ' "${temp_file}" >> "${frac_file}"
        #cat "${frac_file}"

        # Call gulp_coords.py for Actual Conversion
        python ~/bin/gulp_coords.py "$cell" "${frac_file}"
        cp "${gulp_file}.cart" "${gulp_file}.cp2k"

        project_name="${gulp_file}"
        coord_file="${gulp_file}.cp2k"
        abc="$(echo "$cell" | tr -s ' ' | cut -d ' ' -f 1-3)"
        angles="$(echo "$cell" | tr -s ' ' | cut -d ' ' -f 4-6)"
        restart_file="${gulp_file}-1.restart"
        # Make All The Replacements
        sed -e "s/PH_project_name/${project_name}/g" \
            -e "s/PH_coord_file/${coord_file}/g" \
            -e "s/PH_abc/${abc}/g" \
            -e "s/PH_angles/${angles}/g" \
            -e "s/PH_restart_file/${restart_file}/g" \
            -e "s/PH_scf_guess/${scf_guess}/g" \
            -e "s/PH_converge_to/${converge_to}/g" \
            -e "s/PH_inner_convergence/${inner_convergence}/g" \
            -e "s/PH_outer_convergence/${outer_convergence}/g" \
            -e "s/PH_minimiser/${minimiser}/g" \
            -e "s/PH_preconditioner/${preconditioner}/g" \
            -e "s/PH_xc/${xc}/g" \
            -e "s/PH_max_inner_cycles/${max_inner_cycles}/g" \
            -e "s/PH_max_outer_cycles/${max_outer_cycles}/g" \
            -e "s/PH_cutoff/${cutoff}/g" \
            -e "s/PH_rel_cutoff/${rel_cutoff}/g" \
            -e "s/PH_extrapolation/${extrapolation}/g" \
            -e "s/PH_run_type/${run_type}/g" \
            $template_file > $work_dir/$template_file
    done
done
