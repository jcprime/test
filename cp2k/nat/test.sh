#!/bin/bash

for dir in [0-9][0-9]; do
    if [ -d "$dir" ]; then
        cd "$dir" || continue
        for subdir in nat*; do
            if [ -d "$subdir" ]; then
                cd "$subdir" || continue
                rm '*.pdb-final.pdb'
                gunzip "$subdir"-trajectory-pos-1.pdb.gz
                ~/bin/lastpdbstep.sh "$subdir"-trajectory-pos-1.pdb
                cd ..
            fi
        done
        cd ..
    fi
done
