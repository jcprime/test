#!/bin/bash
# setting up the environment for SGE:
#$ -cwd -V
#$ -m e
#$ -M james@master.xenon.local
#$ -pe smp 4 
#$ -N nat05_N3
#$ -e nat05_N3.error
#$ -o nat05_N3.output

/opt/openmpi/gfortran/1.6.5/bin/mpirun --mca btl ^tcp -n 4 /usr/local/cp2k/2.6.0/bin/cp2k.popt nat05_N3.inp > nat05_N3.log

