#!/bin/bash
# setting up the environment for SGE:
#$ -cwd -V
#$ -m e
#$ -M james@master.xenon.local
#$ -pe smp 4 
#$ -N nat11
#$ -e nat11.error
#$ -o nat11.output

/opt/openmpi/gfortran/1.6.5/bin/mpirun --mca btl ^tcp -n 4 /usr/local/cp2k/2.6.0/bin/cp2k.popt nat11.inp > nat11.log

