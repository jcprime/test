#!/bin/bash
# setting up the environment for SGE:
#$ -cwd -V
#$ -m e
#$ -M james@master.xenon.local
#$ -pe smp 4 
#$ -N nat03_N4
#$ -e nat03_N4.error
#$ -o nat03_N4.output

/opt/openmpi/gfortran/1.6.5/bin/mpirun --mca btl ^tcp -n 4 /usr/local/cp2k/2.6.0/bin/cp2k.popt nat03_N4.inp > nat03_N4.log

