#!/bin/bash

home="$(pwd)"
echo "Starting directory is $home"

for numdir in *; do
    echo "Moving into $numdir directory"
    cd "$numdir"
    for dir in *; do
        if [ -d "$dir" ]; then
            step="${dir%%_*}" && step="${step##[a-z]*[^0-9]}"
            if [ ! -d "$step" ]; then
                echo "Making $step directory" && mkdir "$step"
            else
                echo "Directory exists"
            fi
            echo "Moving $dir stuff into $step directory"
            mv "$dir"* "$step"/
        fi
    done
    echo "Moving back into starting $home directory"
    cd "$home"
done
