#!/usr/bin/awk -f
{
	if ($2 ~ /core/ || $2 ~ /shel/) {
		if ($3 ~ /\//) {
			frac3 = $3
			print "Fraction:", frac3
			split(frac3,splitfrac3,"/")
			print "Split fraction:", splitfrac3[1], splitfrac3[2]
			deci3 = (splitfrac3[1])/(splitfrac3[2])
			printf("%.7d",deci3)
		}
		if ($4 ~ /\//) {
			frac4 = $4
			split(frac4,splitfrac4,"/")
			deci4 = (splitfrac4[1])/(splitfrac4[2])
			printf("%.7d",deci4)
		}
		if ($5 ~ /\//) {
			frac5 = $5
			split(frac5,splitfrac5,"/")
			deci5 = (splitfrac5[1])/(splitfrac5[2])
			printf("%.7d",deci5)
		}
		#split(frac,splitfrac,"/")
		#decifluff = (splitfrac[1])/(splitfrac[2])
		#printf("%.7d",decifluff)
		next
	}
	else {
		next
	}
}
#fraction_coords.gin
