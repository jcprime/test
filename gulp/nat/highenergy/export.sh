#!/bin/bash

cd ~/data/test/nat/gs #Move to directory containing the specific rank-ordered output text files
mkdir ~/data/test/nat/structures

for num in {00..20}; do #Loops over dehydration steps
 grab_dump=$(ls nat"$num"_*.dump)
 cd ~/data/test/nat/structures
 echo "Exporting" "$grab_dump" "structure in gs directory, via Jmol, as" "$grab_dump".jpg "image of optimised, in current directory."
 # Exports JPEG of visualised structure
 jmol -i -g 700x657 ~/data/test/nat/gs/"grab_dump" -s ~/binX/jmol_export.spt -w JPG:"${grab_dump%.*}".jpg -x #Call Jmol to export the structure associated with the given rank to a .jpg file of the same name in the current directory, and then exit when finished
done
