Laumontite dehydration simulations, at -0.050GPa (= -50MPa) pressure

*   `ene` directory holds `.tsv` files for each dehydration step, containing the output of `grep 'Final en' *out | sort -n -k5`
*   `gs` directory holds the lowest-energy files at every dehydration step
*   `test` directory holds the test output of `~/bin/gulp_coords.py`
