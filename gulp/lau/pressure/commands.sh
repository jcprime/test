#!/bin/bash
cp orig/lau_n00050.gin n00050/lau00.gin
cp orig/lau_n00100.gin n00100/lau00.gin
cp orig/lau_n00200.gin n00200/lau00.gin
cp orig/lau_n00500.gin n00500/lau00.gin
cp orig/lau_n01000.gin n01000/lau00.gin
cp orig/lau_n02000.gin n02000/lau00.gin
cp orig/lau_n03000.gin n03000/lau00.gin
cp orig/lau_n04000.gin n04000/lau00.gin
cp orig/lau_n05000.gin n05000/lau00.gin
cp orig/lau_n06000.gin n06000/lau00.gin
cp orig/lau_n07000.gin n07000/lau00.gin
cp orig/lau_n08000.gin n08000/lau00.gin
cp orig/lau_n09000.gin n09000/lau00.gin
cp orig/lau_n10000.gin n10000/lau00.gin
cp orig/lau_p00050.gin p00050/lau00.gin
cp orig/lau_p00100.gin p00100/lau00.gin
cp orig/lau_p00200.gin p00200/lau00.gin
cp orig/lau_p00500.gin p00500/lau00.gin
cp orig/lau_p01000.gin p01000/lau00.gin
cp orig/lau_p02000.gin p02000/lau00.gin
cp orig/lau_p03000.gin p03000/lau00.gin
cp orig/lau_p04000.gin p04000/lau00.gin
cp orig/lau_p05000.gin p05000/lau00.gin
cp orig/lau_p06000.gin p06000/lau00.gin
cp orig/lau_p07000.gin p07000/lau00.gin
cp orig/lau_p08000.gin p08000/lau00.gin
cp orig/lau_p09000.gin p09000/lau00.gin
cp orig/lau_p10000.gin p10000/lau00.gin

# If fails occur in the namings, this takes the last part of the filename after the underscore (which should be the dehydrate-created "N[::digit::]+".gin part), and attaches it to "lau00_" to remove unnecessary extra bits
#for dir in [np][0-9]*; do cd "$dir"; for file in *.gin; do new_suffix="${file##*_}"; mv "$file" lau00_"$new_suffix"; done; cd ..; done

# For the very starting ones (pre-dehydrated.sh-ing)
#for dir in [np][0-9]*; do cd "$dir"; dehydrate lau00.gin; cd ..; done

# Sort out output and dump lines for the very starting ones
#for file in [np]*/lau00.gin; do echo "$file"; sed -e "s/dump every   1 an52_n[0-9]*.dump/dump every 1 lau00.dump/g" "$file" > "${file%%.*}".tmp; mv "${file%%.*}".tmp "$file"; done
#for file in [np]*/lau00.gin; do echo "$file"; sed -e "s/output xtl an52_n[0-9]*.xtl/output xtl lau00.xtl/g" "$file" > "${file%%.*}".tmp; mv "${file%%.*}".tmp "$file"; done

# Move any existing fail-named ones to what they should be for very starting ones
#for file in [np]*/an52*.dump; do mv "$file" "${file%%/*}/lau00.dump"; done
#for file in [np]*/an52*.xtl; do mv "$file" "${file%%/*}/lau00.xtl"; done

# Queue the very starting ones
#for dir in [np][0-9]*; do cd "$dir"; for file in lau00*.gin; do qgulp "$file"; done ; cd ..; done

# Move original files (fully-hydrated ones) to respective 00 directories before dehydrated.sh-ing (or else they'll always be higher in energy than the dehydrated ones!)
#for file in [np]*/lau00.*; do mv "$file" "${file%%/*}/00/lau00.gin${file##*/}"; done
