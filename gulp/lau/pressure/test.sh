#!/bin/bash

for dir in [np][0-9]*; do
    cd "$HOME/test/lau/pressure/$dir" || continue
    mkdir "$HOME/test/lau/pressure/$dir/structures"
    echo "Made structures directory"
    cd "$HOME/test/lau/pressure/$dir/gs" || continue
    for file in *.gout; do
        base="${file##*/}"
        if [[ -f "$HOME/test/lau/pressure/$dir/structures/${base%%.*}.jpg" ]]; then
            continue
        fi
        jmol -i -g 700x657 "$file" -s "$HOME/bin/jmol_export.spt" -w JPG:"${file%%.*}.jpg" -x
        mv "${file%%.*}.jpg" "$HOME/test/lau/pressure/$dir/structures/"
    done
    cd "$HOME"
done
