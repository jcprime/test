#!/usr/bin/python
# gulp_coords.py
# Takes coordinates and cell parameters from GULP input files
# and turns them into CP2K-compatible input

import sys
import math

# Function to print usage
def print_usage():
    print "Usage: python gulp_coords.py a b c alpha beta gamma inputFile.ext"

# Function to find and store all lines after "cell"
# but before "end" or "species"

# Function to find and store cell parameters
def cellParameters(a, b, c, alpha, beta, gamma):
    lengths = []
    for i in range(1, 3):
        lengths.append(float(sys.argv[i]))
    angles = []
    for i in range(4, 6):
        angles.append(float(sys.argv[i]))
    return lengths, angles

# Function to convert from fractional to Cartesian coordinates
def fracToCart(lengths, angles, xyz):
    # lengths and angles are lists, each with three elements
    # xyz is a list of floats with the fractional coordinates for that given line
    atom = xyz[0]
    volume = math.sqrt(1 - (math.cos(angles[0]))**2 - (math.cos(angles[1]))**2 - (math.cos(angles[2]))**2 + (2 * math.cos(angles[0]) * math.cos(angles[1]) * math.cos(angles[2])))
    # Use matrix from Wikipedia to convert to Cartesian coordinates
    xCart = (lengths[0] * xyz[1]) + ((lengths[1] * math.cos(angles[2])) * xyz[2]) + ((lengths[2] * math.cos(angles[1])) * xyz[3])
    yCart = (0 * xyz[1]) + ((lengths[1] * math.sin(angles[2])) * xyz[2]) + ((lengths[2] * ((math.cos(angles[0]) - (math.cos(angles[1]) * math.cos(angles[2]))) / math.sin(angles[2]))) * xyz[3])
    zCart = (0 * xyz[1]) + (0 * xyz[2]) + ((lengths[2] * (volume / math.sin(angles[2]))) * xyz[3])
    return [atom, xCart, yCart, zCart]

# Make list of lengths
lengths = []
for i in range(1, 4):
    lengths.append(float(sys.argv[i]))

# Make list of angles, assuming given in degrees, and convert them to radians for use in fracToCart()
angles = []
for i in range(4, 7):
    angles.append(math.radians(float(sys.argv[i])))

reader = open(sys.argv[7], 'r')
sourceFile = reader.readlines()
reader.close()

filename = sys.argv[7].split('.')

# Ensure all fractional coordinates are actually floats
coords = []
for i in sourceFile:
    coord = i.split()
    coord[1] = float(coord[1])
    coord[2] = float(coord[2])
    coord[3] = float(coord[3])
    coords.append(coord)

# Convert the input coordinates into the other type
print "Converting to Cartesian coordinates..."
newCoords = []
for line in coords:
    newCoord = fracToCart(lengths, angles, line)
    newCoords.append(newCoord)

# Create the output strings and write them to the file
outputData = []
for line in newCoords:
    formattedLine = "{0[0]:<2}   {0[1]:> 10.6f}   {0[2]:> 10.6f}   {0[3]:> 10.6f}\n".format(line)
    outputData.append(formattedLine)
outputData.append('\n')

if (filename[-1] == "frac"):
    outputFilename = filename[0] + ".cart"

writer = open(outputFilename, 'w')
writer.write(''.join(outputData))
writer.close()

print "... aaaand we're done."
