#!/bin/bash
# gulp_convert.sh
# - Takes cell parameters and fractional coordinates from GULP input file
# - Reformats them as XYZ-type coordinates
# - Pastes the cell parameters in the appropriate section of the input file

# If "-h" or "--help" is given when running the script as only argument, print usage information
if [[ ( "$#" -eq "1" ) && ( ( "$1" -eq "-h" ) || ( "$1" -eq "--help" ) ) ]]; then
    echo "Usage: gulp_convert.sh [coordinate_file] [input_file]"
fi

#declare -a zeolites=("nat" "sco" "mes")

# Keep count of the number of coordinate files you're writing out!
coord_number="0"

# Loop over zeolites
if [[ "$#" -gt "2" ]]; then
    file="$3"
else
    read -p "File to convert please: " file
fi
    # Set working environment
    work_dir="$(pwd)"
    cd "${work_dir}" || exit
    # Set coord_total to the total number of .gin files in all directories for the given zeolite (i.e. how many CP2K coordinate files you want to have overall by the end)
    # Loop over existing GULP coordinates (in .gin files)
    #for file in ${gin_dir}/[[:digit:]]*/*.gin; do
        base="${file%%.*}"
        # Set output-coordinate file to first argument, if given, or else taken as same basename as original .gin file
        if [[ "$#" -ge "1" ]]; then
            coordinate_file="$1"
        elif [[ "$#" -eq "0" ]]; then
            coordinate_file="${base}.cp2k"
        fi
        # Set cp2k-input file to second argument, if given, or else taken as same basename as original .gin file
        if [[ "$#" -ge "1" ]]; then
            input_file="$2"
        elif [[ "$#" -eq "0" ]]; then
            input_file="${base}.inp"
        fi
        # Grab cell parameters into a new variable
        cell=$(grep -A1 'cell' "${file}" | tail -n 1)
        echo "Cell parameters: ${cell}"

        # Create temporary file
        temp_file="${base}.tmp"
        > "${temp_file}"

        # THIS IS SPECIFIC TO UNIX/LINUX SYSTEMS, AND WON'T WORK ON MAC OS X:
        # Output everything including "fractional" and "totalenergy" OR "species" (whichever comes first) to a temporary file for processing in the while loop to follow
        sed -n "/^fractional/,/^\(space\|totalenergy\|species\)/p" "${file}" >> "${temp_file}"

        frac_file="${base}.frac"
        > "${frac_file}"
        # Remove first and last lines using sed
        sed '1d;$d' "${temp_file}" >> "${frac_file}"
        cat "${frac_file}"
    #done
