#!/bin/bash
# setting up the environment for SGE:
#$ -cwd -V
#$ -m e
#$ -M james@master.xenon.local
#$ -q all.q
#$ -pe smp 1 
#$ -N s
#$ -e s.error
#$ -o s.output

/opt/openmpi/gfortran/1.6.5/bin/mpirun --mca btl ^tcp -n 1 /usr/local/cp2k/2.6.0/bin/cp2k.popt s > s.log

