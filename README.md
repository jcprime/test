# README.md

## Subdirectories

`analysis`: `.txt`, `.csv`, `.ssv`, and `.tsv` files used for data analysis
`coords`: attempts to convert the `GULP` input files into `cp2k` input and coordinate files
`gnuplot`: attempts to plot... stuff, in `gnuplot` (not quite got the hang of that one yet)
`knat`: as-yet-unattempted directory for `GULP`ing potassium-exchanged natrolite (K-NAT)
`lau`: `GULP`ing laumontite
`leo`: `GULP`ing leonhardite
`mes`: `GULP`ing mesolite
`nat`: `GULP`ing natrolite
`orig`: safe place for original `.gin` files to hide
`sco`: `GULP`ing scolecite
`water`: `GULP`ing single water molecule for dehydration-energy calculations
`zzzz`: miscellaneous stuff that isn't likely to be used much (if at all), but that I don't want to delete
